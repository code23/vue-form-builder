import { faEdit } from "@fortawesome/free-solid-svg-icons/faEdit";
import { faCalculator } from "@fortawesome/free-solid-svg-icons/faCalculator";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons/faCalendarAlt";
import { faClock } from "@fortawesome/free-solid-svg-icons/faClock";
import { faDatabase } from "@fortawesome/free-solid-svg-icons/faDatabase";
import { faCheck } from "@fortawesome/free-solid-svg-icons/faCheck";
import { faVenusMars } from "@fortawesome/free-solid-svg-icons/faVenusMars";
import { faUserCircle } from "@fortawesome/free-solid-svg-icons/faUserCircle";
import { faUserShield } from "@fortawesome/free-solid-svg-icons/faUserShield";
import { faGlobeAmericas } from "@fortawesome/free-solid-svg-icons/faGlobeAmericas";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons/faInfoCircle";
import { faFileUpload } from "@fortawesome/free-solid-svg-icons/faFileUpload";
import { faFileImage } from "@fortawesome/free-solid-svg-icons/faFileImage";
import { faFilePdf } from "@fortawesome/free-solid-svg-icons/faFilePdf";
import { faPoundSign } from "@fortawesome/free-solid-svg-icons/faPoundSign";
import { faEquals } from "@fortawesome/free-solid-svg-icons/faEquals";
import { faDotCircle } from "@fortawesome/free-solid-svg-icons/faDotCircle";

var FORM_CONSTANTS = {};
var CONTROL_CONSTANTS = {};
var GENDERS = {};

FORM_CONSTANTS.SectionLayout = {
    collapse: "Collapse",
    tab: "Tab"
    // inner: "Inner Parent",
};

FORM_CONSTANTS.Section = {
    name: "",
    label: "",
    clientKey: "",
    order: 0,
    rows: [],

    // config
    labelPosition: "left", // left or top

    // for dynamic
    isDynamic: false,
    minInstance: 1,
    maxInstance: 0, // 0 for unlimited
    instances: [], // for save data in GUI to easily to retrieve @@

    // page break
    pagebreak: false,

    // attr for tags
    tags: []
};

FORM_CONSTANTS.Row = {
    name: "",
    label: "",
    order: 0,
    controls: []
};

FORM_CONSTANTS.Control = {
    type: "",
    name: "",
    fieldName: "",
    label: "",
    order: 0,
    defaultValue: "",
    value: "",
    className: "col-md-12",
    readonly: false,

    // label style
    labelBold: false,
    labelItalic: false,
    labelUnderline: false,

    // validation
    required: true,

    // attr for text
    isMultiLine: false,

    // attr for number
    isInteger: false,
    decimalPlace: 0,

    // attr for datePicker
    isTodayValue: false,
    dateFormat: "dd/mm/yy",

    // attr for timePicker
    isNowTimeValue: false,
    timeFormat: "HH:mm", // enhancement later

    // attr for select
    isMultiple: false,
    isAjax: false, // is ajax data source or not
    dataOptions: [], // static data source
    ajaxDataUrl: "", // ajax data source

    // attr for checkbox
    isChecked: false,

    // attr for tags
    tags: [],

    // attr for info
    infoControlFieldDefaultValue: "Info field: change text in Settings / Info Text.",

    // SHOW IF logic
    showIf: {
        enabled: false,
        item: "",
        condition: "",
        value: ""
    }
};

FORM_CONSTANTS.Type = {
    info: {
        label: "Info Text",
        icon: faInfoCircle
    },
    text: {
        label: "Text Input",
        icon: faEdit
    },
    number: {
        label: "Number Input",
        icon: faCalculator
    },
    amount: {
        label: "Amount Input",
        icon: faPoundSign
    },
    total: {
        label: "Totals",
        icon: faEquals
    },
    datepicker: {
        label: "Date Picker",
        icon: faCalendarAlt
    },
    timepicker: {
        label: "Time Picker",
        icon: faClock
    },
    select: {
        label: "Select Option",
        icon: faDatabase
    },
    checkbox: {
        label: "Checkbox",
        icon: faCheck
    },
    yesno: {
        label: "Yes/No",
        icon: faDotCircle
    },
    gender: {
        label: "Gender Select",
        icon: faVenusMars
    },
    title: {
        label: "Title Select",
        icon: faUserCircle
    },
    guardian: {
        label: "Guardian Select",
        icon: faUserShield
    },
    country: {
        label: "Country Select",
        icon: faGlobeAmericas
    },
    file: {
        label: "Upload file",
        icon: faFilePdf
    }
};

FORM_CONSTANTS.WidthOptions = {
    // "col-md-1": "Width 1 parts",
    // "col-md-2": "Width 2 parts",
    "col-md-3": "Width 3 parts",
    "col-md-4": "Width 4 parts",
    // "col-md-5": "Width 5 parts",
    "col-md-6": "Width 6 parts",
    // "col-md-7": "Width 7 parts",
    "col-md-8": "Width 8 parts",
    "col-md-9": "Width 9 parts",
    // "col-md-10": "Width 10 parts",
    // "col-md-11": "Width 11 parts",
    "col-md-12": "Width 12 parts"
};

FORM_CONSTANTS.OptionDefault = {
    id: "",
    value: "",
    text: ""
};

CONTROL_CONSTANTS.DateFormat = {
    // rule: date picker format => moment format
    "dd/mm/yy": "D/M/YYYY",
    "dd-mm-yy": "D-M-YYYY",
    "mm/dd/yy": "M/D/YYYY",
    "mm-dd-yy": "M/D/YYYY",
    "yy/mm/dd": "YYYY/M/D",
    "yy-mm-dd": "YYYY-M-D"
};

CONTROL_CONSTANTS.TimeFormat = {
    "H:m": "H:m",
    "HH:mm": "HH:mm",
    "h:m p": "h:m A",
    "hh:mm p": "hh:mm A"
};

export { FORM_CONSTANTS, CONTROL_CONSTANTS };
