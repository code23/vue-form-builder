// icon
import { faEdit } from "@fortawesome/free-solid-svg-icons/faEdit";
import { faCalculator } from "@fortawesome/free-solid-svg-icons/faCalculator";
import { faCalendarAlt } from "@fortawesome/free-solid-svg-icons/faCalendarAlt";
import { faClock } from "@fortawesome/free-solid-svg-icons/faClock";
import { faDatabase } from "@fortawesome/free-solid-svg-icons/faDatabase";
import { faCheck } from "@fortawesome/free-solid-svg-icons/faCheck";
import { faVenusMars } from "@fortawesome/free-solid-svg-icons/faVenusMars";
import { faUserCircle } from "@fortawesome/free-solid-svg-icons/faUserCircle";
import { faUserShield } from "@fortawesome/free-solid-svg-icons/faUserShield";
import { faGlobeAmericas } from "@fortawesome/free-solid-svg-icons/faGlobeAmericas";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons/faInfoCircle";
import { faFileUpload } from "@fortawesome/free-solid-svg-icons/faFileUpload";
import { faFileImage } from "@fortawesome/free-solid-svg-icons/faFileImage";
import { faFilePdf } from "@fortawesome/free-solid-svg-icons/faFilePdf";
import { faPoundSign } from "@fortawesome/free-solid-svg-icons/faPoundSign";
import { faEquals } from "@fortawesome/free-solid-svg-icons/faEquals";
import { faDotCircle } from "@fortawesome/free-solid-svg-icons/faDotCircle";

// GUI Control
import GUITextControl from "code23VueFormBuilder/gui/ui/controls/TextControl";
import GUIInfoControl from "code23VueFormBuilder/gui/ui/controls/InfoControl";
import GUINumberControl from "code23VueFormBuilder/gui/ui/controls/NumberControl";
import GUIAmountControl from "code23VueFormBuilder/gui/ui/controls/AmountControl";
import GUITotalControl from "code23VueFormBuilder/gui/ui/controls/TotalControl";
import GUIDatePickerControl from "code23VueFormBuilder/gui/ui/controls/DatePickerControl";
import GUITimePickerControl from "code23VueFormBuilder/gui/ui/controls/TimePickerControl";
import GUISelectControl from "code23VueFormBuilder/gui/ui/controls/SelectControl";
import GUICheckboxControl from "code23VueFormBuilder/gui/ui/controls/CheckboxControl";
import GUIYesnoControl from "code23VueFormBuilder/gui/ui/controls/YesnoControl";
import GUIGenderControl from "code23VueFormBuilder/gui/ui/controls/GenderControl";
import GUITitleControl from "code23VueFormBuilder/gui/ui/controls/TitleControl";
import GUIGuardianControl from "code23VueFormBuilder/gui/ui/controls/GuardianControl";
import GUICountryControl from "code23VueFormBuilder/gui/ui/controls/CountryControl";
import GUIFileUploadControl from "code23VueFormBuilder/gui/ui/controls/FileUploadControl";

// TEMPLATE Control
import TEMPLATETextControl from "code23VueFormBuilder/template/ui/common/controls/TextControl";
import TEMPLATEInfoControl from "code23VueFormBuilder/template/ui/common/controls/InfoControl";
import TEMPLATENumberControl from "code23VueFormBuilder/template/ui/common/controls/NumberControl";
import TEMPLATEAmountControl from "code23VueFormBuilder/template/ui/common/controls/AmountControl";
import TEMPLATETotalControl from "code23VueFormBuilder/template/ui/common/controls/TotalControl";
import TEMPLATEDatePickerControl from "code23VueFormBuilder/template/ui/common/controls/DatePickerControl";
import TEMPLATETimePickerControl from "code23VueFormBuilder/template/ui/common/controls/TimePickerControl";
import TEMPLATESelectControl from "code23VueFormBuilder/template/ui/common/controls/SelectControl";
import TEMPLATECheckboxControl from "code23VueFormBuilder/template/ui/common/controls/CheckboxControl";
import TEMPLATEYesnoControl from "code23VueFormBuilder/template/ui/common/controls/YesnoControl";
import TEMPLATEGenderControl from "code23VueFormBuilder/template/ui/common/controls/GenderControl";
import TEMPLATETitleControl from "code23VueFormBuilder/template/ui/common/controls/TitleControl";
import TEMPLATEGuardianControl from "code23VueFormBuilder/template/ui/common/controls/GuardianControl";
import TEMPLATECountryControl from "code23VueFormBuilder/template/ui/common/controls/CountryControl";
import TEMPLATEFileUploadControl from "code23VueFormBuilder/template/ui/common/controls/FileUploadControl";

// CONFIG CONTROL
import SIDEBARTextControl from "code23VueFormBuilder/template/ui/sidebar_items/TextConfigComponent";
import SIDEBARInfoControl from "code23VueFormBuilder/template/ui/sidebar_items/InfoConfigComponent";
import SIDEBARNumberControl from "code23VueFormBuilder/template/ui/sidebar_items/NumberConfigComponent";
import SIDEBARAmountControl from "code23VueFormBuilder/template/ui/sidebar_items/AmountConfigComponent";
import SIDEBARTotalControl from "code23VueFormBuilder/template/ui/sidebar_items/TotalConfigComponent";
import SIDEBARDatePickerControl from "code23VueFormBuilder/template/ui/sidebar_items/DatePickerConfigComponent";
import SIDEBARTimePickerControl from "code23VueFormBuilder/template/ui/sidebar_items/TimePickerConfigComponent";
import SIDEBARSelectControl from "code23VueFormBuilder/template/ui/sidebar_items/SelectConfigComponent";
import SIDEBARCheckboxControl from "code23VueFormBuilder/template/ui/sidebar_items/CheckboxConfigComponent";
import SIDEBARYesnoControl from "code23VueFormBuilder/template/ui/sidebar_items/YesnoConfigComponent";
import SIDEBARGenderControl from "code23VueFormBuilder/template/ui/sidebar_items/GenderConfigComponent";
import SIDEBARTitleControl from "code23VueFormBuilder/template/ui/sidebar_items/TitleConfigComponent";
import SIDEBARGuardianControl from "code23VueFormBuilder/template/ui/sidebar_items/GuardianConfigComponent";
import SIDEBARCountryControl from "code23VueFormBuilder/template/ui/sidebar_items/CountryConfigComponent";
import SIDEBARFileUploadControl from "code23VueFormBuilder/template/ui/sidebar_items/FileUploadConfigComponent";

const CONTROL_TYPES = {
    info: {
        label: "Info Text",
        icon: faInfoCircle,
        source: {
            gui: GUIInfoControl,
            template: TEMPLATEInfoControl,
            config: SIDEBARInfoControl
        },
        other_properties: {
            required: false,
            readonly: true
        }
    },
    text: {
        label: "Text Input",
        icon: faEdit,
        source: {
            gui: GUITextControl,
            template: TEMPLATETextControl,
            config: SIDEBARTextControl
        },
        other_properties: {
            required: false
        }
    },
    number: {
        label: "Number Input",
        icon: faCalculator,
        source: {
            gui: GUINumberControl,
            template: TEMPLATENumberControl,
            config: SIDEBARNumberControl
        }
    },
    amount: {
        label: "Amount Input",
        icon: faPoundSign,
        source: {
            gui: GUIAmountControl,
            template: TEMPLATEAmountControl,
            config: SIDEBARAmountControl
        }
    },
    total: {
        label: "Totals",
        icon: faEquals,
        source: {
            gui: GUITotalControl,
            template: TEMPLATETotalControl,
            config: SIDEBARTotalControl
        }
    },
    datepicker: {
        label: "Date Picker",
        icon: faCalendarAlt,
        source: {
            gui: GUIDatePickerControl,
            template: TEMPLATEDatePickerControl,
            config: SIDEBARDatePickerControl
        }
    },
    timepicker: {
        label: "Time Picker",
        icon: faClock,
        source: {
            gui: GUITimePickerControl,
            template: TEMPLATETimePickerControl,
            config: SIDEBARTimePickerControl
        }
    },
    select: {
        label: "Select Option",
        icon: faDatabase,
        source: {
            gui: GUISelectControl,
            template: TEMPLATESelectControl,
            config: SIDEBARSelectControl
        }
    },
    checkbox: {
        label: "Checkbox",
        icon: faCheck,
        source: {
            gui: GUICheckboxControl,
            template: TEMPLATECheckboxControl,
            config: SIDEBARCheckboxControl
        }
    },
    yesno: {
        label: "Yes/No",
        icon: faDotCircle,
        source: {
            gui: GUIYesnoControl,
            template: TEMPLATEYesnoControl,
            config: SIDEBARYesnoControl
        }
    },
    gender: {
        label: "Gender Select",
        icon: faVenusMars,
        source: {
            gui: GUIGenderControl,
            template: TEMPLATEGenderControl,
            config: SIDEBARGenderControl
        }
    },
    title: {
        label: "Title Select",
        icon: faUserCircle,
        source: {
            gui: GUITitleControl,
            template: TEMPLATETitleControl,
            config: SIDEBARTitleControl
        }
    },
    guardian: {
        label: "Guardian Select",
        icon: faUserShield,
        source: {
            gui: GUIGuardianControl,
            template: TEMPLATEGuardianControl,
            config: SIDEBARGuardianControl
        }
    },
    country: {
        label: "Country Select",
        icon: faGlobeAmericas,
        source: {
            gui: GUICountryControl,
            template: TEMPLATECountryControl,
            config: SIDEBARCountryControl
        }
    },
    file: {
        label: "File Upload",
        icon: faFilePdf,
        source: {
            gui: GUIFileUploadControl,
            template: TEMPLATEFileUploadControl,
            config: SIDEBARFileUploadControl
        }
    }
};

export { CONTROL_TYPES };
